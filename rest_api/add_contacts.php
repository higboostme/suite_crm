<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
$conn = mysqli_connect("localhost","root","","suite_crm");

if(!$conn){
    die("Connection failed: " . mysqli_connect_error());
	http_response_code(404);
}
$input = json_decode(file_get_contents("php://input"));
$sql = "INSERT INTO contacts (id, first_name, last_name) VALUES('" . $input->id . "', '" . $input->first_name . "', '" . $input->last_name . "')";

if (mysqli_query($conn, $sql)) {
    $return['status']  = true;
	$return['message'] = 'New record inserted successfully.';
	http_response_code(201);
} else {
    echo "Error: " . $sql . "<br>" . $conn->error;
}

header('Content-Type: application/json');
echo json_encode($return, JSON_PRETTY_PRINT);
$conn->close();?>
