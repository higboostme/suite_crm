<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
$conn = mysqli_connect("localhost","root","","suite_crm");

if(!$conn){
    die("Connection failed: " . mysqli_connect_error());
	http_response_code(404);
}
$input = json_decode(file_get_contents("php://input"));
$sql = "UPDATE contacts SET first_name = '" . $input->first_name . "', last_name = '" . $input->last_name . "' WHERE id = '" . $input->id . "'";

if (mysqli_query($conn, $sql)) {
    $return['status']  = true;
	$return['message'] = 'Record updated successfully.';
	http_response_code(202);
} else {
    echo "Error: " . $sql . "<br>" . $conn->error;
}
$sql = "SELECT * FROM contacts WHERE id = '" . $input->id . "'";

if($result = mysqli_query($conn, $sql)){
    if(mysqli_num_rows($result) > 0){
        echo "<table>";
            echo "<tr>";
                echo "<th>ID</th>";
                echo "<th>First Name</th>";
                echo "<th>Last Name</th>";
            echo "</tr>";
        while($row = mysqli_fetch_array($result)){
            echo "<tr>";
                echo "<td>" . $row['id'] . "</td>";
                echo "<td>" . $row['first_name'] . "</td>";
                echo "<td>" . $row['last_name'] . "</td>";
            echo "</tr>";
        }
        echo "</table>";
        mysqli_free_result($result);
    } else{
        echo "No records matching your query were found.";
    }
} else {
    echo "Error: " . $sql . "<br>" . $conn->error;
}

header('Content-Type: application/json');
echo json_encode($return, JSON_PRETTY_PRINT);
$conn->close();?>